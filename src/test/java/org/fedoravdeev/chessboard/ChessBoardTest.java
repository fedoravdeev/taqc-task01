package org.fedoravdeev.chessboard;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class ChessBoardTest extends ChessBoard {

    @Test
    @DisplayName("should be print chess board 8 x 8")
    public void printChessBoard8on8() {
        String expected = "* * * * \n" +
                " * * * *\n" +
                "* * * * \n" +
                " * * * *\n" +
                "* * * * \n" +
                " * * * *\n" +
                "* * * * \n" +
                " * * * *";
        ChessBoard chessBoard = new ChessBoard();
        String actual = chessBoard.print(new String[]{"8","8"});
        Assert.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("should be print chess board 3 x 3")
    public void printChessBoard3on3() {
        String expected = "* *\n" +
                " * \n" +
                "* *";
        ChessBoard chessBoard = new ChessBoard();
        String actual = chessBoard.print(new String[]{"3","3"});
        Assert.assertEquals(expected, actual);
    }
}