package org.fedoravdeev.chessboard;

public class Main {
    public static void main(String[] args) {
        ChessBoard chessBoard = new ChessBoard();
        System.out.println(chessBoard.print(args));
    }
}
