package org.fedoravdeev.chessboard;

public class ChessBoard {
    public String print(String[] args) {
        if (args.length == 2) {
            int height = Integer.parseInt(args[0]);
            int width = Integer.parseInt(args[1]);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if ((i + j) % 2 == 0) {
                        stringBuilder.append("*");
                    } else {
                        stringBuilder.append(" ");
                    }
                }
                if (i < height - 1) {
                    stringBuilder.append("\n");
                }
            }
            return (stringBuilder.toString());

        } else {
            printHelp();
        }
        return "";
    }

    private void printHelp() {
        System.out.println("Chessboard\n" +
                "Display a chessboard with given dimensions of height and width, according to the principle:\n" +
                "* * * * \n" +
                " * * * *\n" +
                "* * * * \n" +
                " * * * *\n" +
                "* * * * \n" +
                " * * * *\n" +
                "* * * * \n" +
                " * * * *");
    }
}
